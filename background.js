"use strict";

// DATA DECLARATIONS

var tabs = {};
var blocked = [];
var whitelisted = [];

var whiteListTo = [];
var whiteListFrom = [];

var blackList = [];
var dumpList = [];

var prefix = "Hide Element (Once)";
var cmdBlock = "Block URLs";
var cmdDump = "Auto Dump";
var cmdEdit = "Edit Dumps";

var ready2blockIds = [];
var ready2dumpIds = [];
var ready2editIds = [];

var chromeExtTab = "chrome://extensions/";
var debugLog = false;
var debugLastSwitch = Date.now();
const DEBUG_TIMEOUT = 2000; // 2 seconds
const CHAR_PART_DELIMITER = "·";
const CHAR_PAIRS_LINKER   = "±";
const CHAR_KEY_TO_VALUE   = "≡";

const selectors = ['~', '|', '^', '$', '*'];
// CSS selectors:
// ''  - as is,
// '~' - word,
// '|' - starts with a word,
// '^' - begins with a substring,
// '$' - ends with a substring,
// '*' - substring


// STORAGE STUFF

function setData(name, x)
{
  localStorage[name] = x;
}

function getData(name)
{
  return localStorage[name]; 
}

function restoreList(name, splitter)
{
  var array = window[name];
  var s = getData(name);
  if(s != undefined && s.length > 0)
  {
    log(name + ' read:' + s);
    array.push(...s.split(splitter));
  }
}

function addElementToList(name, add)
{
  var array = window[name];
  if(array && array.indexOf(add) == -1)
  {
    array.push(add);
    var s = array.join('\t');
    setData(name, s);
    log(name + ' saved:' + s);
  }
}

// for output.js
function mergeWhiteList(name, add)
{
  var array = window[name];
  for(var i = 0; i < add.length; i++)
  {
    if(array.indexOf(add[i]) == -1)
    {
      array.push(add[i]);
    }
  }
  var s = array.join();
  log(name + ' saved:' + s);
  setData(name, s);
}

// for manage.js and black.js
function replaceList(name, add, splitter)
{
  var array = window[name];
  array.length = 0;
  //array = add.slice();
  array.push(...add);
  var s = array.join(splitter ? splitter : ',');
  log(name + ' saved:' + s);
  setData(name, s);
}


// URL STUFF

function URL(url)
{
  url = url || "";
  this.parse(url);
}

URL.prototype =
{
  href: "",
  protocol: "",
  host: "",
  hostname: "",
  port: "",
  pathname: "",
  search: "",
  hash: "",
    
  parse: function(url)
  {
    url = url || this.href;
    var pattern = "^(([^:/\\?#]+):)?(//(([^:/\\?#]*)(?::([^/\\?#]*))?))?([^\\?#]*)(\\?([^#]*))?(#(.*))?$";
    var rx = new RegExp(pattern); 
    var parts = rx.exec(url);
    
    this.href = parts[0] || "";
    this.protocol = parts[2] || "";
    this.host = parts[4] || "";
    this.hostname = parts[5] || "";
    this.port = parts[6] || "";
    this.pathname = parts[7] || "/";
    this.search = parts[8] || "";
    this.hash = parts[10] || "";
    
    this.update();
  },
    
  update: function()
  {
    this.host = this.hostname + (("" + this.port) ? ":" + this.port : "");
    this.href = this.protocol + '//' + this.host + this.pathname + this.search + this.hash;
  },
    
  assign: function(url)
  {
    this.parse(url);
  },
  
  similar: function(that)
  {
    if(that instanceof Object && typeof that.hostname == 'string')
    {
      var domains1 = this.hostname.split('.').reverse();
      var domains2 = that.hostname.split('.').reverse();
      for(var i = 0; i < 2; i++) // domains1.length
      {
        if(domains1[i] != domains2[i]) return false;
      }
      return true;
    }
    return false;
  }
}


// MENU MANAGEMENT

var mid = chrome.contextMenus.create(
{
  contexts: ['all'], 
  title: prefix,
  onclick: function(info, tab)
  {
    var fid = (info.frameId ? info.frameId : 0);
    // As per docs, {frameId: fid} should work in the object, but I'm getting:
    // Error in event handler for contextMenus: Error: Invalid value for argument 2. Property 'frameId': Unexpected property. from event_bindings.dispatch
    chrome.tabs.executeScript(tab.id, {code: 'hideElement(' + fid + ',"' + info.frameUrl + '");', allFrames: true});
  }
});

var bid = chrome.contextMenus.create(
{
  contexts: ['all'],
  title: cmdBlock
});

var dmp = chrome.contextMenus.create(
{
  contexts: ['all'],
  title: cmdDump
});

var edit = chrome.contextMenus.create(
{
  contexts: ['all'],
  title: cmdEdit
});
  
function createMenuItem(creationObject, onclickHandler, callback, frameUrl)
{
  if(onclickHandler)
  {
    creationObject.onclick = function(onClickData, tab)
    {
      onclickHandler(onClickData, tab, creationObject, frameUrl);
    };
  }
  var item; 
  try
  {
    item = chrome.contextMenus.create(creationObject, callback);
  }
  catch(e)
  {
    console.error(e);
    console.log(creationObject);
  }
  return item;
}

function removeOldMenuItems(ids)
{
  for(var i = 0; i < ids.length; i++)
  {
    try
    {
      chrome.contextMenus.remove(ids[i]);
    }
    catch(e)
    {
      console.error(e);
    }    
  }
  ids.length = 0;
}


// BLOCKING REQUESTS

function addBlocked(tabId, url)
{
  if(blocked[tabId] == undefined)
  {
    blocked[tabId] = {origin: tabs[tabId], urls:[], domains:[], blacks: 0};
  }
  blocked[tabId].urls.push(url);
  if(url.startsWith("*\t")) blocked[tabId].blacks++;
}

function action(request)
{
  if(request.type == "sub_frame" || request.type == "other")
  {
    //if(debugLog) console.log('placeholder ' + request.url + ' ' + request.type);
    return {redirectUrl: chrome.extension.getURL("empty.html") + '?' + request.url};
  }
  else if(request.type == "image")
  {
    //if(debugLog) console.log('placeholder ' + request.url + ' ' + request.type);
    return {redirectUrl: chrome.extension.getURL("empty.svg") + '?' + request.url};
  }
  else
  {
    /*
    if(confirm("Proceed Ok?"))
    {
      console.log("ok before ", request.url);
    }
    else
    {
      console.log("bad before ", request.url);
      chrome.tabs.sendRequest(request.tabId, {alreadybad: true});
    }
    */
    return {cancel: true};
  }
}

function adjustColor(tabId)
{
  if(blocked[tabId].domains.length > 0)
  {
    if(blocked[tabId].blacks == 0)
    {
      chrome.browserAction.setBadgeBackgroundColor({color: "#FF0000"});
    }
    else
    {
      chrome.browserAction.setBadgeBackgroundColor({color: "#990000"});
    }
  }
  else // assume blocked[tabId].blacks > 0
  {
    chrome.browserAction.setBadgeBackgroundColor({color: "#FFAA00"});
  }
}

function debug(logmethod, request, comment)
{
  logmethod.call(console, request.type, "::", request.tabId, request.frameId, request.parentFrameId, "::", tabs[request.tabId], "->", request.url, comment);
}

function interceptRequest(request)
{
  if(request && request.url)
  {
    if(request.tabId != -1 && request.type == "main_frame")
    {
      tabs[request.tabId] = request.url;
    }

    var urlNext = new URL(request.url);

    if(!tabs[request.tabId])
    {
      if(whiteListTo.indexOf(urlNext.host) != -1)
      {
        if(debugLog) debug(console.log, request, "hidden in whitelist TO");
        return {cancel: false};
      }
      else
      { 
        if(debugLog) debug(console.warn, request, "hidden is not in whitelist TO");
        return {cancel: true};
      }
    }
    
    var urlCurrent = new URL(tabs[request.tabId]);
    
    if(request.type == "main_frame") // new page/site is loading in main window
    {
      if(debugLog) debug(console.log, request, "");

      if(blocked[request.tabId]) delete blocked[request.tabId];
      if(whitelisted[request.tabId]) delete whitelisted[request.tabId];
      
      // icon will be automatically reset to default on every new page
      
      chrome.browserAction.setBadgeText({text:'', tabId:request.tabId});
      
      if(whiteListFrom.indexOf(urlCurrent.host) != -1)
      {
        whitelisted[request.tabId] = true;
        chrome.browserAction.setIcon({path:"domaincage_icon_green_32.png", tabId:request.tabId});
      }

      return {cancel: false};
    }

    // can be chrome: or chrome-extension:, ftp:, etc
    if(urlCurrent.protocol != 'http' && urlCurrent.protocol != 'https')
    {
      if(debugLog) debug(console.log, request, "origin protocol");
      return {cancel: false};
    } 
    if(urlNext.protocol != 'http' && urlNext.protocol != 'https' && urlNext.protocol != '')
    {
      if(debugLog) debug(console.log, request, "target protocol");
      return {cancel: false};
    }
    if(request.type == "stylesheet")
    {
      if(debugLog) debug(console.log, request, "allow CSS");
      return {cancel: false};
    }

    if(ifExist(request.url))
    {
      if(debugLog) debug(console.warn, request, "blocked/redirected by blacklist");
      addBlocked(request.tabId, "*\t" + request.url);
      
      return action(request);
    }
    
    if(whiteListFrom.indexOf(urlCurrent.host) != -1)
    {
      if(debugLog) debug(console.log, request, "whitelist FROM");
      return {cancel: false};
    }

    var filtered = whiteListTo.filter(function(e) { return this.endsWith(e); }, ('.' + urlNext.host)); 
    if(filtered.length > 0)
    {
      if(debugLog) debug(console.log, request, "whitelist TO");
      return {cancel: false};
    }

    if(urlCurrent.similar(urlNext)) // allows calls between subdomains of levels 2+ (from abc.domain.com to xyz.domain.com)
    {
      if(debugLog) debug(console.log, request, "subdomains: " + (urlCurrent.host + " -> " + urlNext.host));
      return {cancel: false};
    }    
    
    if(urlCurrent.host != urlNext.host)
    {
      if(debugLog) debug(console.warn, request, "blocked/redirected external call");
      addBlocked(request.tabId, request.url);
      
      var domain = urlNext.host;
      
      if(blocked[request.tabId].domains.indexOf(domain) == -1)
      {
        blocked[request.tabId].domains.push(domain);
      }
      chrome.browserAction.setIcon({path:"domaincage_icon_red_32.png", tabId:request.tabId});
      adjustColor(request.tabId);
      chrome.browserAction.setBadgeText({text:(blocked[request.tabId].domains.length + blocked[request.tabId].blacks).toString(), tabId:request.tabId});
      return action(request);
    }
  }
  return {cancel: false};
}


// TABS MANAGEMENT

function getTabHost(id)
{
  if(blocked[id])
  {
    var u = new URL(blocked[id].origin);
    return u.host;
  }
  return null;
}

function tabCreated(tab)
{
  if(tab.url)
  {
    if(tab.url == chromeExtTab)
    {
      if(!debugLog)
      {
        if(Date.now() - debugLastSwitch > DEBUG_TIMEOUT)
        {
          log('logging enabled');
          debugLog = true;
          debugLastSwitch = Date.now();
        }
      }
      else
      {
        if(Date.now() - debugLastSwitch > DEBUG_TIMEOUT)
        {
          log('logging disabled');
          debugLog = false;
          debugLastSwitch = Date.now();
        }
      }
    }
    if(debugLog) console.log('created:' + tab.id + ' ' + tab.url);
    tabs[tab.id] = tab.url;
  }
}

function tabUpdated(tabId, changeInfo, tab)
{
  // if changeInfo.url is presented, url has been updated,
  // but in some cases tab.url is filled without this 
  if(tab.url) // it's equal to changeInfo.url
  {
    if(tab.url == chromeExtTab)
    {
      if(!debugLog)
      {
        if(Date.now() - debugLastSwitch > DEBUG_TIMEOUT)
        {
          log('logging enabled');
          debugLog = true;
          debugLastSwitch = Date.now();
        }
      }
      else
      {
        if(Date.now() - debugLastSwitch > DEBUG_TIMEOUT)
        {
          log('logging disabled');
          debugLog = false;
          debugLastSwitch = Date.now();
        }
      }
    }
    if(debugLog) console.log('updated:' + tabId + ' n:' + tab.url + ' o:' + tabs[tabId] + ' ' + blocked[tabId] + ' ' + whitelisted[tabId]);
    if(!tabs[tabId])
    {
      tabs[tabId] = tab.url;
    }
    
    // this event does not fire all the time
    if(blocked[tabId] != undefined)
    {
      chrome.browserAction.setIcon({path:"domaincage_icon_red_32.png", tabId:tabId});
      adjustColor(tabId);
      chrome.browserAction.setBadgeText({text:(blocked[tabId].domains.length + blocked[tabId].blacks).toString(), tabId:tabId});
    }
    else
    if(whitelisted[tabId] != undefined)
    {
      chrome.browserAction.setIcon({path:"domaincage_icon_green_32.png", tabId:tabId});
    }
  }
}

function openTab(filename)
{
  var myid = chrome.i18n.getMessage("@@extension_id");
  chrome.windows.getCurrent(
  function(win)
  {
    chrome.tabs.query({'windowId': win.id},
    function(tabArray)
    {
      for(let t of tabArray)
      {
        if(t.url == "chrome-extension://" + myid + "/" + filename)
        {
          console.log("already opened");
          chrome.tabs.update(t.id, {active: true});
          chrome.tabs.reload(t.id);
          return;
        }
      }
      chrome.tabs.create({url:chrome.extension.getURL(filename)});
    });
  });
}

function log(s)
{
  console.log(s);
}


// HIDING/DUMPING of ELEMENTS

function urlOffset(v) 
{
  for(var i = 0; i < blackList.length; i++)
  {
    if(v.indexOf(blackList[i]) != -1)
    {
      return i;
    }
  }
  return -1;
}

function ifExist(v)
{
  return urlOffset(v) > -1; 
}

function dumpOffset(v) // check the dump-list for current URL/tag/params tuple passed in 'v' (comes from the page context menu)
{
  var tag2attrs = v.split(CHAR_PART_DELIMITER); // url·tag·attr1≡value1±attr2≡value2
  var domain = (new URL(tag2attrs[0])).host;

  for(var i = 0; i < dumpList.length; i++)
  {
    if(dumpList[i].startsWith(tag2attrs[0] + CHAR_PART_DELIMITER)  // url beginning match (usually protocol://domain/)
    || dumpList[i].startsWith("*" + CHAR_PART_DELIMITER) // 'any url' rule
    || (dumpList[i].charAt(0) == "*" && domain.endsWith(dumpList[i].substring(1)))) // subdomain (*.main.com), or domain and subdomain (*main.com)
    {
      var stored = dumpList[i].split(CHAR_PART_DELIMITER);
      if(tag2attrs.length == 2 && stored.length == 2) return i; // only tag name
      if(tag2attrs.length > 2  && stored.length > 2)  // both have attributes
      {
        var attr2valuesFind = tag2attrs[2].split(CHAR_PAIRS_LINKER); // key-value pairs
        var attr2valuesExist = stored[2].split(CHAR_PAIRS_LINKER);
        
        for(var j = 0; j < attr2valuesFind.length; j++)
        {
          var pairFind = attr2valuesFind[j].split(CHAR_KEY_TO_VALUE);
          var index = stored[2].indexOf(pairFind[0] + CHAR_KEY_TO_VALUE); 
          if(index != -1) // online key is present in the base
          {
            for(var k = 0; k < attr2valuesExist.length; k++)
            {
              var pairExist = attr2valuesExist[k].split(CHAR_KEY_TO_VALUE);
              if(pairExist[0] == pairFind[0])
              {
                if(pairFind.length == 1 && pairExist.length == 1) return i; // keys without values matching 
                
                if(pairFind.length == 2 && pairExist.length == 2)
                {
                  if(pairFind[1].charAt(0) == "*") return i; // any value matches

                  // value can start with an optional control char for CSS selector (~, |, ^, $, *)
                  var wordsFind = pairFind[1].split(" ");
                  var wordsBase = pairExist[1].split(" ").map(e => (selectors.indexOf(e.charAt(0)) > -1 ? e.substring(1) : e));
                  
                  if(wordsFind.filter(item => wordsBase.includes(item)).length > 0) return i; // values match
                }
                
                break;
              }
            }
          }
        }
      }
    }
  }
  return -1;
}

function ifDumped(v)
{
  return dumpOffset(v) > -1;
}

function collectDumpsByUrl(url, indexed)
{
  var result = [];
  var domain = (new URL(url)).host;
  for(var i = 0; i < dumpList.length; i++)
  {
    var parts = dumpList[i].split(CHAR_PART_DELIMITER);
    if(parts[0] == "*" // any domain/url
    || url.startsWith(parts[0]) // leading match
    || (parts[0].startsWith("*") && domain.endsWith(parts[0].substring(1)))) // trailing match (subdomains)
    {
      let selector = parts[1]; // tag
      
      if(parts.length == 3)
      {
        var attrs = parts[2].split(CHAR_PAIRS_LINKER); // attr-value pairs
        for(var j = 0; j < attrs.length; j++)
        {
          selector += "[";
          var keyvalue = attrs[j].split(CHAR_KEY_TO_VALUE);
          if(keyvalue.length > 1)
          {
            let s = selectors.indexOf(keyvalue[1].charAt(0));
            selector += keyvalue[0] + ((s > -1) ? selectors[s] : '') + '="' + ((s > -1) ? keyvalue[1].substring(1) : keyvalue[1]) + '"';
          }
          else
          {
            selector += keyvalue[0]; 
          } 
          selector += "]";
        }
      }
        
      if(!indexed)
      {
        result.push(selector);
      }
      else
      {
        result.push({selector: selector, index: i});
      }
    }
  }
  return result;
}


const BASE_EDIT_ITEM_ID = 128000;

chrome.extension.onRequest.addListener(
function(request, sender, callback)
{
  if(request.install)
  {
    var result = {};
    result.dumps = collectDumpsByUrl(sender.url);
    if(sender.tab) result.tabId = sender.tab.id;
    result.frameId = sender.frameId;  
    callback(result);
    return;
  }
  
  if(request.notification)
  {
    chrome.notifications.create("DomainCage",
    {   
      type: 'basic', 
      iconUrl: "domaincage_icon_48.png", 
      title: "Domain Cage", 
      message: request.notification 
    });
    return;
  }

  if(request.target)
  {
    if(debugLog) console.log(request);
    chrome.contextMenus.update(mid, {title: prefix + " " + request.target});
  }
  else
  {
    chrome.contextMenus.update(mid, {title: prefix + " [n/a]"});
  }

  if(!sender.frameId)
  {
    removeOldMenuItems(ready2dumpIds);
    removeOldMenuItems(ready2editIds);
  }
  if(request.dom && request.dom.length > 0)
  {                                                     
    var existing = collectDumpsByUrl(sender.url, true);

    if(sender.frameId)
    {
      request.dom.push({tag: "---", attrs: {}});
      chrome.tabs.sendRequest(sender.tab.id, {frameUrl: sender.url, frameId: sender.frameId, target: request.target, dom: request.dom, stack: request.stack, exist: existing});
      return;
    }
    
    if(request.exist)
    {
      existing.push(...request.exist.filter((e) => !existing.some((x) => e.index == x.index)));
    }          
    
    var n = request.dom.length;
    chrome.contextMenus.update(dmp, {title: cmdDump + " [" + n + "]"});

    if(debugLog)
    {
      console.log(request.dom);
      console.log(existing);
    }

    for(var i = 0; i < n; i++)
    {
      var t = request.dom[i].tag + CHAR_PART_DELIMITER + Object.keys(request.dom[i].attrs).map(key => key + CHAR_KEY_TO_VALUE + request.dom[i].attrs[key]).join(CHAR_PAIRS_LINKER); 
      ready2dumpIds.push(
        createMenuItem(
        {
          contexts: ['all'],
          title: t,
          parentId: dmp,
          type: request.dom[i].tag == "---" ? 'separator' : 'checkbox', // this is for verification of correct work (if checked item shows up - there is a problem (domains/iframes/exceptions))
          checked: ifDumped(sender.url + CHAR_PART_DELIMITER + t)
        },
        function(info, tab, init, frameUrl)
        {
          var url = new URL(frameUrl || sender.url);
          var link = url.protocol + '://' + url.host + url.pathname;
        
          if(debugLog) console.log('clicked:' + info.menuItemId + " " + init.title);
          if(init.checked) // should not occur normally
          {
            var x = dumpOffset(link + CHAR_PART_DELIMITER + init.title);
            if(confirm("Delete existing record '" + dumpList[x] + "'?"))
            {
              dumpList.splice(x, 1);
              setData("dumpList", dumpList.join('\t'));
            }
            return;
          }

          var text = prompt("Please edit selector for permanent dumping", link + CHAR_PART_DELIMITER + init.title);
          if(text != null)
          {
            addElementToList("dumpList", text);
            chrome.notifications.create("DomainCage",
            {   
              type: 'basic', 
              iconUrl: "domaincage_icon_48.png", 
              title: "Domain Cage", 
              message: "New record will take effect after page reload" 
            });
          }
        },
        function()
        {
        },
        request.dom[i].frameUrl)
      );
    }
    
    if(existing.length > 0)
    {
      chrome.contextMenus.update(edit, {title: cmdEdit + " [" + existing.length + "]", onclick: null});
    }
    else
    {
      chrome.contextMenus.update(edit, {title: cmdEdit,
        onclick: function(info, tab)
        {
          openTab("black.html");
        }
      });
    }
    
    for(var i = 0; i < existing.length; i++)
    {
      ready2editIds.push(
        createMenuItem(
        {
          contexts: ['all'],
          title: existing[i].selector,
          parentId: edit,
          id: (existing[i].index + BASE_EDIT_ITEM_ID).toString()
        },
        function(info, tab, init, dummy)
        {
          if(debugLog) console.log('clicked:' + info.menuItemId + " (" + (typeof info.menuItemId) + ") " + init.title);
          var x = info.menuItemId - BASE_EDIT_ITEM_ID;
          if(confirm("Delete existing record '" + dumpList[x] + "'?"))
          {
            dumpList.splice(x, 1);
            setData("dumpList", dumpList.join('\t'));
          }
        },
        function()
        {
        })
      );
    }
    
  }
  else
  {
    chrome.contextMenus.update(dmp, {title: cmdDump + " [n/a]"});
    chrome.contextMenus.update(edit, {title: cmdEdit + " [n/a]"});
  }
  
  removeOldMenuItems(ready2blockIds);
  if(request.stack && request.stack.length > 0)
  {
    chrome.contextMenus.update(bid, {title: cmdBlock + " [" + request.stack.length + "]"});
    
    if(debugLog) console.log(request.stack);
    var n = request.stack.length;
    for(var i = 0; i < n; i++)
    {
      ready2blockIds.push(
        createMenuItem(
        {
          contexts: ['all'],
          title: request.stack[i],
          parentId: bid,
          type: 'checkbox',
          checked: ifExist(request.stack[i])
        },
        function(info, tab, init, dummy)
        {
          if(debugLog) console.log('clicked:' + info.menuItemId + " " + init.title);
          if(init.checked)
          {
            var x = urlOffset(init.title);
            if(confirm("Delete existing record '" + blackList[x] + "'?"))
            {
              blackList.splice(x, 1);
              setData("blackList", blackList.join('\t'));
            }
            return;
          }
          
          var text = prompt("Please edit URL to block in future", init.title);
          if(text != null)
          {
            addElementToList("blackList", text);
          }
        },
        function()
        {
        })
      );
    }
  }
  else
  {
    chrome.contextMenus.update(bid, {title: cmdBlock + " [n/a]"}); // type: "separator"
  }
});


// MAIN

restoreList("whiteListTo", ",");
restoreList("whiteListFrom", ",");
restoreList("blackList", '\t');
restoreList("dumpList", '\t');

chrome.tabs.onCreated.addListener(tabCreated);
chrome.tabs.onUpdated.addListener(tabUpdated);
chrome.webRequest.onBeforeRequest.addListener(interceptRequest, {urls: ["*://*/*"]}, ['blocking']);

document.title = "DomainCage";