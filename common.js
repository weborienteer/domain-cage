"use strict";

function add2Table(s, main, p1, p2, control, counter)
{
  var div = document.getElementById(main);
  var parent = document.createElement('div');
  parent.id = p1 + s;
  parent.setAttribute('domain', s);
  div.appendChild(parent);

  var element = document.createElement('input');
  element.type = "checkbox";
  element.id = p2 + s;
  element.onclick = checkboxClickedFactory(p2, control, counter);
  
  var label = document.createElement('label');
  label.setAttribute('for', element.id);
  label.innerHTML = s;
  
  parent.appendChild(element);
  parent.appendChild(label);
}

function checkboxClickedFactory(prefix, control, counter)
{
  counter.count = counter.count || 0; 
  function _checkboxClicked(event)
  {
    if(this.id.substr(0, 2) == prefix)
    {
      if(event.altKey)
      {
        this.checked = false;
        var text = prompt("Please edit the item", event.target.parentElement.innerText);
        if(text != null)
        {
          event.target.parentElement.setAttribute('domain', text);
          if(typeof window[control] === "function")
          {
            window[control].call();
            location.reload();
          }
        }
        return;
      }
      if(this.checked) counter.count++;
      else counter.count--;
      document.getElementById(control).disabled = (counter.count == 0);
    }
  }
  return _checkboxClicked; 
}

function add2TableTo(s, control, counter)
{
  add2Table(s, 'main1', 'p_', 'c_', control, counter);
}

function add2TableFrom(s, control, counter)
{
  add2Table(s, 'main2', 'h_', 's_', control, counter);
}

function remove(main, prefix)
{
  var div = document.getElementById(main);
  var child = div.firstChild;
  var a = [];
  while(child)
  {
    if(child.tagName && child.tagName.toLowerCase() == 'div')
    {
      if(child.className == undefined || (child.className.toLowerCase() != 'header'))
      {
        var d = child.getAttribute('domain');
        // console.log(d);
        var check = document.getElementById(prefix + d);
        if(check && check.checked)
        {
          check.checked = false;
          child.style.display = 'none';
        }
        else
        {
          a.push(d);
        }
      }
    }
    child = child.nextSibling;
  }
  return a;
}
