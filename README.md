# README #

Domain Cage is a Chrome extension started back in 2008 as a completely new project inspired by eponymous Firefox extension. The sources are ready to install and run in Chrome using `Settings` -> `Extensions ` -> `Load unpacked extension` button. The packed version (signed `.crx` file) for everyday use can be obtained from the [Downloads](https://bitbucket.org/weborienteer/domain-cage/downloads) section. Downloads may have a bit outdated version in comparison to the sources.


### Quick summary ###

Domain Cage can block web requests to all external domains except for the cases, when originating site or target site is white-listed. Also requests to black-listed URLs can be blocked. Starting from version 0.8, it also allows you to kill unwanted html elements automatically after page load.

Please find details about setup and usage in the [Wiki](https://bitbucket.org/weborienteer/domain-cage/wiki/Home).


### Installation ###

Copy contents of the repository to any place on your local disk, then `Load unpacked extension` in Chrome from the chosen place.

The code is self-contained, there are no external dependencies.


### Changes ###

The latest version is 0.8. 

Changes in version 0.8.0:

*  New feature of automatic dumping/removal of predefined html-elements after page load. The elements for dumping can be selected through the context menu. Conditions for dumping are defined in a subset of CSS selectors. List of dumps are managed on the page with blacklisted content (click "Black" button in the popup);
*  Frames processing for all nesting levels from the event target up to the main page;
*  White and block lists can be edited inplace (Alt + Click);
*  Code refactoring;
*  Multiple fixes;

Changes in version 0.8.1:

*  Requests from background service workers are allowed only if specified in the whitelist TO;
*  Leading wildcard symbol '*' is supported in dumping records to include subdomains;
*  New feature is added to remove or highlight minimal suspicious containers with external links (target=\_blank). Lists (UL, OL, role="list", role="navigation") are excluded from candidates. Removal is enabled by custom dumping selector "![\_]", highlighting - by "![~]"; Try it on sites with "tags randomization";
*  An experimental attempt is made to force context menus on elements which prevent default behaviour;
*  Duplication of menu items from subframes is fixed;

Changes in version 0.8.2:

*  More improvements to enforce context menu when it's disabled by site code;
*  Fixed a bug with Hide Element command, which did not work in subframes;
*  Hide Element can now repeatedly hide containers of selected element upon user confirmation;

Changes in version 0.8.3:

*  Improvements: more extensive check for DOM changes after mutations, allow all CSS, small refactoring.


### Known limitations ###

*   Some sites are now mangling tag names and attributes randomly in order to prevent detection of applicable signatures for element dumping. For such sites stable configuration of the Dumping feature is not currently possible.
*   Some sites shows video ads in the same video-element which is then used for actual content. The Dumping feature is useless here, try to block content by request URLs instead.
*   I use JavaScript prompt's and confirm's as interactive UI where user feedback is required, for example, after he/she selects an element in the context menu. This is a necessary measure, because Chrome developers did not provide means for programmable popup display (intentionally!). Hence we don't have a decent API for interaction with user in the context of an external webpage, without leaving current tab/window. You may report this as a bug to Chrome team.
