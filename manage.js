"use strict";

var counterTo = {};
var counterFrom = {};

function readStorage()
{
  var a = chrome.extension.getBackgroundPage().whiteListTo;
  for(var i = 0; i < a.length; i++)
  {
    add2TableTo(a[i], 'removeTo', counterTo);
  }
  var b = chrome.extension.getBackgroundPage().whiteListFrom;
  for(var i = 0; i < b.length; i++)
  {
    add2TableFrom(b[i], 'removeFrom', counterFrom);
  }
}

function removeTo()
{
  chrome.extension.getBackgroundPage().replaceList("whiteListTo", remove('main1', 'c_'));
  document.getElementById('removeTo').disabled = true;
}

function removeFrom()
{
  chrome.extension.getBackgroundPage().replaceList("whiteListFrom", remove('main2', 's_'));
  document.getElementById('removeFrom').disabled = true;
}

document.addEventListener('DOMContentLoaded',
function()
{
  readStorage();
  document.getElementById('removeTo').addEventListener('click', removeTo);
  document.getElementById('removeFrom').addEventListener('click', removeFrom);
});
