"use strict";

var whiteListTo = {};
var checkboxChecked = 0;
var initialHeight;

function update(s)
{
  document.getElementById('tableau').innerHTML = s;
  document.getElementById('edit').addEventListener('click', editWhiteLists);
  document.getElementById('edit2').addEventListener('click', editBlackLists);
}

function bindListeners()
{
  document.getElementById('allow').addEventListener('click',
  function(e)
  {
    submitOrigin(e.target.getAttribute('domain'));
  });
  var a = document.querySelectorAll('input[type="checkbox"]')
  for(var i = 0; i < a.length; i++)
  {
    a[i].addEventListener('change',
    function(e)
    {
      checkboxClicked(e.target);
    });
  }
  document.getElementById('custominput').addEventListener('change',
  function(e)
  {
    writeCustomItem();
  });
  document.getElementById('white').addEventListener('click', submitWhitelist);
  document.getElementById('more').addEventListener('click', showLinks);
  // document.getElementById('links').addEventListener('click', hideLinks);
  var links = document.getElementsByClassName('links');
  for(var i = 0; i < links.length; i++)
  {
    links[i].addEventListener('click', switchMoreLess);
  }
}

function showDummy()
{
  chrome.windows.getCurrent(
  function(win)
  { 
    chrome.tabs.query({'windowId': win.id, 'active': true},
    function(tabArray)
    {
      var blocked = chrome.extension.getBackgroundPage().blocked[tabArray[0].id];
      if(blocked != undefined)
      {
        var domain = chrome.extension.getBackgroundPage().getTabHost(tabArray[0].id);
 
        var s = 'The following content has been blocked in <b>' + domain + '</b><br />';
        s += '<p style="white-space: nowrap;"><input type="button" id="allow" value="Allow all requests from this host" domain="' + domain + '"><input type="button" id="edit" value="White"><input type="button" id="edit2" value="Black"></p>';
        s += '<div id="domains" style="border:1px dotted;padding:5px;width:340px;"><b>Domains</b>:<br />'; // overflow-x:scroll;
        for(var i = 0; i < blocked.domains.length; i++)
        {
          s += '<input type="checkbox" id="' + blocked.domains[i] + '"><label for="' + blocked.domains[i] + '">' + blocked.domains[i] + '</label><br />';
        }
        s += '<input type="checkbox" id="customcheck"><input type="edit" id="custominput" placeholder="(custom value)"><br /><span style="font-size:8pt">Leading . is allowed as subdomain mask (.domain.com)</span><br />';
        s += '<input type="button" id="white" disabled="true" value="Allow requests to selected domains"><input type="checkbox" id="reload" checked="true" tabid="' + tabArray[0].id + '"><label for="reload">Reload</label></div>';
        s += '<div id="more" style="cursor:pointer">Click here to view all links</div>';
        s += '<div id="links" style="display:none;border:1px dotted;padding:5px;width:340px;"><b>Links: (blacklisted fragments are in bold)</b><br />';
        checkboxChecked = 0;

        for(var i = 0; i < blocked.urls.length; i++)
        {
          if(blocked.urls[i].indexOf('*\t') == 0)
          {
            var x = chrome.extension.getBackgroundPage().urlOffset(blocked.urls[i]);
            var b = chrome.extension.getBackgroundPage().blackList[x];
            var p = blocked.urls[i].indexOf(b);
            s += '<div class="links">' + blocked.urls[i].substr(2, p - 2) + '<b style="pointer-events: none;">' + blocked.urls[i].substr(p, b.length) + '</b>' + blocked.urls[i].substr(p + b.length) + '</div>';   
          }
          else
          {
            s += '<div class="links">' + blocked.urls[i] + '</div>';
          }
        }
        
        s += '</div>';
        update(s);
        bindListeners();
      }
      else
      {
        var s = 'There is no blocked content in this page<br />';
        s += '<p><input type="button" id="edit" value="Whitelist"><input type="button" id="edit2" value="Blacklist"></p>';
        update(s);
      }
    }); 
  });
}

function log(s)
{
  chrome.extension.getBackgroundPage().log(s);
}

function showLinks(event)
{
  document.getElementById('links').style.display = "block";
  event.target.style.display = "none";
  initialHeight = document.body.clientHeight + "px";
}

function hideLinks(event)
{
  document.getElementById('more').style.display = "block";
  document.getElementById('links').style.display = "none";
}

function switchMoreLess(event)
{
  if(event.target.style.height == 'auto')
  {
    event.target.style.height = '';
    event.target.readonly = undefined;
  }
  else
  {
    event.target.style.height = 'auto';
    event.target.readonly = 'true';
  }
}

function checkboxClicked(x)
{
  if(x.id == "reload") return;
  
  if(x.checked)
  {
    checkboxChecked++;
    if(x.id == "customcheck")
    {
      var e = document.getElementById('custominput');
      if(e.value !== '(custom value)' && e.value.length > 0)
      {
        whiteListTo[x.id] = e.value;
      }
    }
    else
    {
      whiteListTo[x.id] = x.id;
    }
  }
  else
  {
    checkboxChecked--;
    delete whiteListTo[x.id];
  }
  
  var button = document.getElementById('white');
  button.disabled = checkboxChecked  == 0; 
}

function writeCustomItem()
{
  var c = document.getElementById('customcheck')
  if(c.checked)
  {
    var e = document.getElementById('custominput');
    if(e.value.length > 0)
    {
      whiteListTo['customcheck'] = e.value;
    }
  }
}

function reload()
{
  var c = document.getElementById('reload');
  if(c.checked)
  {
    chrome.tabs.reload(parseInt(c.getAttribute('tabid')), {bypassCache: true});
  }
}

function submitWhitelist()
{
  chrome.extension.getBackgroundPage().mergeWhiteList("whiteListTo", values(whiteListTo));
  reload();
  window.close();
}

function submitOrigin(s)
{
  chrome.extension.getBackgroundPage().mergeWhiteList("whiteListFrom", [s]);
  reload();
  window.close();
}

function values(obj)
{
  var values = [];

  for(var key in obj)
  {
    if(obj.hasOwnProperty(key))
    {
      values.push(obj[key]);
    }
  }

  return values;
}

function keys(obj)
{
  var keys = [];

  for(var key in obj)
  {
    if(obj.hasOwnProperty(key))
    {
      keys.push(key);
    }
  }

  return keys;
}

function editWhiteLists()
{
  chrome.extension.getBackgroundPage().openTab("manage.html");
  window.close();
}

function editBlackLists()
{
  chrome.extension.getBackgroundPage().openTab("black.html");
  window.close();
}

document.addEventListener('DOMContentLoaded',
function()
{
  showDummy();
});
