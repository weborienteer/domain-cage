"use strict";

var counterTo = {};
var counterFrom = {};

function readStorage()
{
  var a = chrome.extension.getBackgroundPage().blackList;
  for(var i = 0; i < a.length; i++)
  {
    add2TableTo(a[i], 'removeTo', counterTo);
  }
  var b = chrome.extension.getBackgroundPage().dumpList;
  for(var i = 0; i < b.length; i++)
  {
    add2TableFrom(b[i], 'removeFrom', counterFrom);
  }
}

function removeTo()
{
  chrome.extension.getBackgroundPage().replaceList("blackList", remove('main1', 'c_'), '\t');
  document.getElementById('removeTo').disabled = true;
}

function removeFrom()
{
  chrome.extension.getBackgroundPage().replaceList("dumpList", remove('main2', 's_'), '\t');
  document.getElementById('removeFrom').disabled = true;
}

document.addEventListener('DOMContentLoaded',
function()
{
  readStorage();
  document.getElementById('removeTo').addEventListener('click', removeTo);
  document.getElementById('removeFrom').addEventListener('click', removeFrom);
});
