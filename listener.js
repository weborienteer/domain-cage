"use strict";

var last_target = null;

var dumpArray;
var frameId;
var tabId; 

const tags = ["a","abbr","address","area","article","aside","audio","b","base","bdi","bdo","blockquote","body","br","button","canvas","caption","center","cite","code","col","colgroup","data","datalist","dd","del","details","dfn","dialog","div","dl","dt","em","embed","fieldset","figcaption","figure","footer","form","h1","h2","h3","h4","h5","h6","head","header","hgroup","hr","html","i","iframe","img","input","ins","kbd","label","legend","li","link","main","map","mark","menu","meta","meter","nav","noindex","noscript","object","ol","optgroup","option","output","p","param","picture","pre","progress","q","rp","rt","ruby","s","samp","script","section","select","slot","small","source","span","strong","style","sub","summary","sup","table","tbody","td","template","textarea","tfoot","th","thead","time","title","tr","track","u","ul","var","video","wbr",

"animate","animateMotion","animateTransform","audio","canvas","circle","clipPath","defs","desc","discard","ellipse","feBlend","feColorMatrix","feComponentTransfer","feComposite","feConvolveMatrix","feDiffuseLighting","feDisplacementMap","feDistantLight","feDropShadow","feFlood","feFuncA","feFuncB","feFuncG","feFuncR","feGaussianBlur","feImage","feMerge","feMergeNode","feMorphology","feOffset","fePointLight","feSpecularLighting","feSpotLight","feTile","feTurbulence","filter","foreignObject","g","iframe","image","line","linearGradient","marker","mask","metadata","mpath","path","pattern","polygon","polyline","radialGradient","rect","script","set","stop","style","svg","switch","symbol","text","textPath","title","tspan","unknown","use","video","view"];

function checkBlankTargets(node, highlight)
{
  var array = Array.from(node.querySelectorAll("A[target=_blank][href^=http]:not([rel=nofollow]):not([rel=noopener])"));
  var n = array.length, m = 0;
  var previous, parent;
  for(let element of array)
  {
    previous = null;
    
    if(window.getComputedStyle(element, null).getPropertyValue("display") == "inline") continue;
     
    parent = element.parentNode;
    
    do
    {
      if(["navigation", "list"].indexOf(parent.getAttribute("role") || "*") != -1) previous = null;
      if(parent.querySelector("A:not([target=_blank])")) break;
      previous = parent;
    }
    while(!!(parent = parent.parentNode));
    
    if(previous && previous.tagName.toUpperCase() != "LI")
    {
      // TODO: analyze aria attributes
      m++;
      if(!highlight)
      {
        previous.remove();
      }
      else
      {
        previous.style.backgroundColor = "yellow";
        previous.style.borderWidth = "thick";
        previous.style.borderColor = "red";
        previous.style.opacity = "0.25";
      }
    }
  }
  return {removed: m, total: n};
}

function filterNodeByDumpConditions(node, dumpArray, isDynamic)
{
  let caption = isDynamic ? "Dynamic" : "Static";
  for(let condition of dumpArray)
  {
    if(condition.charAt(0) == "!") // control statements first
    {
      if(condition == "![_]" || condition == "![~]") // remove or highlight
      {
        var r = checkBlankTargets(node, condition.charAt(2) == '~');
        if(r.removed > 0) console.log(caption + " blank targets removed:", r.removed, "of", r.total);
      }
    }
    else if(condition.charAt(0) == "?") // non-standard tags second
    {
      var addedTree = Array.from(node.getElementsByTagName("*"));
      var unknown = addedTree.filter((e) => tags.indexOf(e.tagName.toLowerCase()) == -1);
      if(unknown.length > 0)
      {
        console.log(caption + " dumping", unknown.length, "non-standard tags");
        var result = {str: ""};
        unknown.forEach(function(e){this.str += e.tagName + ";"; e.remove();}, result);
        console.log(result.str);
      }
    }
    else // standard tags at last
    {
      var array = Array.from(node.querySelectorAll(condition)); // matches
      if(array.length > 0)
      {
        console.log(caption + " removing " + array.length + " elements for " + condition); 
        array.forEach(e => e.remove());
        if(!isDynamic) window.dispatchEvent(new Event('resize'));
      }
    }
  }
}

var observer = new MutationObserver(mutations =>
{
  if(!dumpArray) return; // somehow this can be undefined sometimes

  for(let mutation of mutations)
  {
    if(!mutation.addedNodes || mutation.addedNodes.length == 0) continue;
    for(let node of Array.from(mutation.addedNodes))
    {
      if(!(node instanceof Element)) continue;

      if(node instanceof HTMLStyleElement) continue;
      // console.log(node);

      // now checking entire body instead of node, cause some changes are not trapped otherwise
      filterNodeByDumpConditions(document.body, dumpArray, true);
      
      // content scripts are not injected into src-less iframes, so process them explicitly
      var frames = Array.from(document.querySelectorAll('iframe'));
      for(let frame of frames)
      {
        if(!frame.src && frame.contentDocument)
        {
          filterNodeByDumpConditions(frame.contentDocument, dumpArray, true);
        }
      }
      
      // since we're now checking entire document, not need to loop further
      return;
    }
  }  
});


var trigger = false;
var hack = false;

function installHandler()
{
  document.addEventListener('contextmenu', function capturing(event)
  {
    // console.log("capturing", event);
    
    var duplicate = new event.constructor(event.type, event);
    duplicate.subclassed = true;
    if(hack)
    {
      // console.log("hacking...");
      event.target.dispatchEvent(duplicate);
    }
    else
    {
      trigger = true;
      setTimeout(function()
      {
        if(trigger)
        {
          console.log("Context menu was prevented to appear, try again");
          chrome.extension.sendRequest({notification: "Context menu was prevented to appear, try again"});
          
          trigger = false;
          hack = true;
          event.target.dispatchEvent(duplicate);
        }
      }, 500);
    }
  }, true);

  document.addEventListener('contextmenu', function bubbling(event)
  {
    // console.log("bubbling", event);
    if(!event.defaultPrevented)
    {
      trigger = false;
      hack = false;
    }
  }, false);

  chrome.extension.sendRequest({install: true}, function(responce)
  {
    dumpArray = responce.dumps;
    frameId = responce.frameId;
    tabId = responce.tabId;  
    console.log('Tab:', tabId, ', frame:', frameId);
    console.log('Url:', document.location.href);
    console.log('Got:', dumpArray.length, 'rule(s)');
    if(dumpArray.length > 0)
    {
      console.log(dumpArray);
      filterNodeByDumpConditions(document.body, dumpArray, false);
    }
  });

  document.addEventListener('mousedown', down, true);

  observer.observe(document.body,
  {
    childList: true,
    subtree: true,
  });

  console.log('DomainCage injected in ' + document.location.href);
}

function collectParentLinks(element)
{
  var result = [];
 
  do
  {
    if(element.src || element.href || element.URL)
    {
      result.push(element.src || element.href || element.URL);
    }
    if(element.style)
    {
      var str = window.getComputedStyle(element, null).getPropertyValue("background");
      if(str)
      {
        var pos = str.indexOf('url("');
        if(pos != -1)
        {
          result.push(str.substring(pos + 5, str.indexOf('")', pos)));
        }
      }
    }
  }
  while(!!(element = element.parentNode));
  return result;
}

function collectElement(element, result)
{
  var obj;
  if(element.attributes)
  {
    obj = {};    
    var attrs = element.attributes;
    for(var i = attrs.length - 1; i >= 0; i--)
    {
      obj[attrs[i].name] = attrs[i].value; 
    }
  }

  if(element.tagName)
  {
    if(frameId)
    {
      result.push({tag: element.tagName, attrs: obj, frameUrl: document.location.href});
    }
    else
    {
      result.push({tag: element.tagName, attrs: obj});
    }
  }
}

function collectDOMProperties(element)
{
  var result = [];
 
  do
  {
    collectElement(element, result);
  }
  while(!!(element = element.parentNode));
  return result;
}

function collectArrayProperties(array)
{
  var result = [];
 
  for(let element of array)
  {
    collectElement(element, result);
  }
  return result;
}

function down(event)
{
  if(event.button == 2 && event.target instanceof Element)
  {
    // console.log('target:' + event.target.tagName + ' (' + event.clientX + ',' + event.clientY + ') ' + event.target.src + ' ' + event.target.href);
    // console.log(event);
    
    last_target = event.target;
    // last_target.style.backgroundColor = "yellow";
    var r = collectParentLinks(event.target);
    var d = collectArrayProperties(event.path); 
     // || collectArrayProperties(document.elementsFromPoint(event.clientX, event.clientY));
    
    chrome.extension.sendRequest({target: event.target.tagName, stack: r, dom: d}); // , frame: {id: frameId, url: document.location.href}
    
    // for objects we just check that the mouse button == 2
    if(event.target.tagName.toLowerCase() == "embed")
    {
      hideElement();
    }
    
    //checkBlankTargets(event.target.parentNode.parentNode); // debug
  }
}

function hideElement(fid, furl)
{
  console.log(fid, furl);
  if(fid == frameId || furl == document.location.href)
  {
    for(;;)
    {
      var next; 
      if(last_target)
      {
        next = last_target.parentNode; 
        last_target.remove(); // last_target.style.display = 'none';
      }
      var selObj = document.getSelection();
      if(selObj)
      {
        selObj.deleteFromDocument();
      }
  
      if(next)
      {
        var result = [];
        collectElement(next, result);
        if(result.length > 0)
        {    
          if(confirm("Proceed to next container? " + next.tagName + " " + JSON.stringify(result[0].attrs)))
          {
            last_target = next;
            continue;        
          }
        }
      }
      break;
    }
  }
}

document.addEventListener("DOMContentLoaded", installHandler);

chrome.extension.onRequest.addListener(
function(request, sender, callback)
{
  if(request.alreadybad)
  {
    console.log("SPOILED");
  }
  else
  if(request.frameUrl && request.frameId != frameId)
  {
    // console.log(request);
    var frames = document.getElementsByTagName("iframe");
    for(let f of Array.from(frames))
    {
      // console.log(f);
      if(f.src === request.frameUrl)
      {
        var r = collectParentLinks(f);
        var d = collectDOMProperties(f);
        request.stack.push(...r.filter((e) => request.stack.indexOf(e) == -1));
        request.dom.push(...d);
        
        chrome.extension.sendRequest({target: request.target, stack: request.stack, dom: request.dom, exist: request.exist});

        return;
      }
    }
    //console.log("Frame", frameId, "can't find subframe", request.frameUrl, "in", document.location.href);    
  }
});

/* this is too late (original listeners from the page is already attached)

//Event.prototype.stopPropagation = function(){};
//Event.prototype.preventDefault = function(){};

var oldAddEventListener = EventTarget.prototype.addEventListener;

EventTarget.prototype.addEventListener = function(eventName, eventHandler)
{
  oldAddEventListener.call(this, eventName, function(event)
  {
    if(eventName == 'contextmenu')
    {
      console.log("trap", event);
    }
    eventHandler(event);
  });
};
*/